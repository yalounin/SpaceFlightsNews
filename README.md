Тестовое приложение для Surf Studio - "Space Flights News".

Цель: показ данных с сервера в таблице и, при нажатии на строчку - на отдельном экране. Данные кэшируются в БД устройства, делая приложение автономным.

API: https://api.spaceflightnewsapi.net/v3/documentation#/Article/get_articles 

Stack:
Swift
UIKit
URLSession
CoreData
Codable

Architecture: Model-View-Controller
Code-style: https://github.com/surfstudio/SwiftCodestyle 
