
import UIKit

// класс для экрана просмотра новости
final class ItemViewController: UIViewController {

    // аутлеты изображения и текста
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsLabel: UILabel!
    // свойства для заполнения аутлетов
    var newsImage: Data?
    var newsText: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        guard let data = newsImage else {
            return
        }
        // отображаем на экране полученные данные
        newsLabel.text = newsText
        newsImageView.image = UIImage(data: data)
    }
}
