
import Foundation

// модель для парсинга данных с сервера
final class NewsJSON: Codable {
    var imageUrl: String
    var summary: String
}
