
import Foundation

// класс для получения данных с сервера и кэширования их в БД
final class DataProvider {
    // создаем экземпляры сервисов
    let server = ServerService()
    let db = DBService()

    // получить кжшированные данные
    func fetchData(completion: (([News]) -> Void)?) {
        var newsArray = [News]() // создаем пустой массив

        // делаем запрос на сервер
        server.fetchData(completion: { [weak self] news in
            guard let strongSelf = self else {
                return
            }
            // если полученные данные не nil, то обновляем БД
            if let news = news {
                strongSelf.db.cacheData(news)
            }
            // получаем данные из БД и передаем в комплишн
            newsArray = strongSelf.db.getObjects()
            completion?(newsArray)
        })
    }
}
