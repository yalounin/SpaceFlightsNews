
import UIKit

// класс для получения данных с сервера
final class ServerService {
    // url в строковом формате
    let urlString = "https://api.spaceflightnewsapi.net/v3/articles?_limit=4"
    // формируем запрос
    var request: URLRequest {
        return URLRequest(url: URL(string: urlString)!)
    }
    
    // получить данные
    func fetchData(completion: (([NewsJSON]?) -> Void)?) {

        // делаем запрос на сервер
        let task = URLSession.shared.dataTask(with: request) { data, _, error in
            // в случае ошибки или отсутствия данных, передаем в комплишн nil
            guard let data = data, error == nil else {
                completion?(nil)
                return
            }
            // декодируем через Codable-модель
            let news = try? JSONDecoder().decode([NewsJSON].self, from: data)
            completion?(news) // передаем модель в комплишн
        }
        task.resume()
    }
}
