

import CoreData
import UIKit

// класс для кэширования и получения данных в БД
final class DBService {
    // получаем контекст
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var context: NSManagedObjectContext {
        return appDelegate.persistentContainer.viewContext
    }
    
    // кэшировать данные
    func cacheData(_ news: [NewsJSON]) {
        guard let entity = NSEntityDescription.entity(forEntityName: "News", in: context) else {
            return
        }
        
        clear() // перед кэшем очищаем БД

        // для каждого объекта массива создаем объект БД
        for value in news {
            guard let url = URL(string: value.imageUrl) else {
                return
            }
            let newsObject = News(entity: entity, insertInto: context)
            // передаем данные
            newsObject.newsText = value.summary
            // преобразовываем String в Data для открытия изображений по url
            let data = try? Data(contentsOf: url)
            newsObject.imageData = data
            
            // сохраняем контекст
            do {
                try context.save()
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    // получить данные из БД
    func getObjects() -> [News] {
        var news = [News]() // создаем пустой массив
        
        let fetchRequest: NSFetchRequest<News> = News.fetchRequest()
        do {
            news = try context.fetch(fetchRequest) // заполняем массив данными из контекста
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return news
    }
    
    // очистить БД
    func clear() {
        // получаем данные и создаем запрос на удаление
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "News")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try context.execute(deleteRequest) // удаляем все объекты
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}
