
import UIKit
// основной VC с таблицей новостей
final class MainViewController: UIViewController {

    var news = [News]() // массив данных
    var provider = DataProvider() // провайдер для получения данных
    @IBOutlet weak var tableView: UITableView! // аутлет таблицы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // назначаем делегата и источника данных для таблицы
        tableView.delegate = self
        tableView.dataSource = self
        tableView.alpha = 0 // скрываем строки до получения данных
        
        // отображаем индикатор загрузки
        let indicator = UIActivityIndicatorView(style: .medium)
        view.addSubview(indicator)
        indicator.center = tableView.center
        indicator.startAnimating()

        // получаем данные в провайдере (в фоновом потоке)
        DispatchQueue.global().async {
            self.provider.fetchData(completion: { [weak self] news in
                guard let strongSelf = self else {
                    return
                }
            strongSelf.news = news // обновляем массив и перезагружаем таблицу
            DispatchQueue.main.async {
                strongSelf.tableView.alpha = 1
                indicator.stopAnimating()
                strongSelf.tableView.reloadData()
            }
        })
    }
    }
}

// подписываемся на протоколы tableView
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count // количество элементов массива
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // инициализируем строки
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? TableViewCell else {
            return UITableViewCell()
        }
        let n = news[indexPath.row]
        cell.fillData(from: n) // заполняем строки данными
        return cell
    }
    
    // действие при нажатии на строчку
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // инициализируем второй VC
        guard let vc = storyboard?.instantiateViewController(identifier: "ItemViewController") as? ItemViewController else {
            return
        }
        // передаем ему данные
        let item = news[indexPath.row]
        vc.newsImage = item.imageData
        vc.newsText = item.newsText
        // открываем его
        navigationController?.pushViewController(vc, animated: true)
    }
}
