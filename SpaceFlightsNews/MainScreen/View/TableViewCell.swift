
import CoreData
import UIKit

// класс для отрисовки строки таблицы
final class TableViewCell: UITableViewCell {

    // аутлеты элементов вью
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsLabel: UILabel!
    
    // заполнить строку данными из объекта news
    func fillData(from news: News) {
        guard let data = news.imageData else {
            return
        }
        newsLabel.text = news.newsText
        newsImageView.image = UIImage(data: data)
    }
    
    // перед реюзом передаем изображению значение nil, чтобы они не накладывались друг на друга
    override func prepareForReuse() {
        newsImageView.image = nil
    }
}
